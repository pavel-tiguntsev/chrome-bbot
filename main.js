var user = null;
var socket = io.connect('http://localhost:3000');
var thisTab = {};

function $gid(id){
  return document.getElementById(id);
}

function onAnchorClick(event) {
    chrome.tabs.create({
      selected: true,
      url: event.srcElement.href
    });
    console.log('Bbot...');
    return false;
  }
  


  function loginPanel() {
    if (user != null) {
      console.log('user')
    } 
  }

  function logged(user) {
    $gid("loginPanel").style.display = 'none';
    $gid("username-el").innerHTML = user;
    initConnection(user);
  }

  function auth() {
    user = $gid('inp-username').value ;
    if ($gid('addRand').checked == true) {
      user = user + "_" + String.fromCharCode(Math.floor(Math.random() * 20 + 65)).repeat(Math.floor(Math.random() * 4 + 1))
    }
    Cookies.set('user', user);
    logged(user)
  }

  function logOut() {
    Cookies.remove('user');
    $gid("loginPanel").style.display = '';
    $gid("username-el").innerHTML = '';
  }


  function buildTree(socket, array) {
    var tree = []

    for(var i = 0; i < array.length; i++){
      if (array[i].children != undefined) {
        console.log(array[i].children);
        
        var category = {id: array[i].id, title: array[i].title, parentId: array[i].parentId};
        buildTree(socket, array[i].children)
        socket.emit('addtree', {
          "tree": category,
          "user": user
        });
      }
    }
  }


  function sendHistory(socket, username) {
    var history;
    var microsecondsPerWeek = 1000 * 60 * 60 * 24 * 7;
    var oneWeekAgo = (new Date).getTime() - microsecondsPerWeek;
    console.log((new Date));
    chrome.bookmarks.getTree((tree) => {
      buildTree(socket, tree[0].children);
    });

    chrome.history.search({
      'text': '',
      'startTime': oneWeekAgo
    },
    function(history) {
      var historySorted = history.sort(function(a, b){
        return b.visitCount-a.visitCount
        })

      chrome.tabs.getSelected(null,function(tab) {
        thisTab = tab;
        var msgText;
        var newMsg = false;
        console.log(tab);
        
        var site = thisTab.url.split('/')[2];
        if (site == "www.redtube.com") {
          chrome.tabs.executeScript(null, {file: "content_script.js"});
          var msgText = 'Опять - redtube.com. \n  \n Не самый лучший сайт для тебя сейчас...  \n  Дело твое, но давай лучше найдем девочку по твоиму вкусу.  \n  Я наблюдал какие видео ты смотришь, мы подберем по лицу и телу. Привезем к тебе домой, мы знаем где ты живешь. \n \n   Пообучаешь мою нейронку и получить от меня BBT Токены, \n которыми и оплатишь заказ.'
          newMsg = true;
        }
        if (newMsg) {
          addtoChat({
            from: 0,
            msg: msgText
          })
        }
        
      });


      function reTrain(data){
        console.log(data);
      }

      chrome.bookmarks.onCreated.addListener(function(data){
        console.log('asdasdasd');
        console.log(data);
      });
      // chrome.bookmarks.onMoved((data) => {reTrain(data)});

      chrome.topSites.get(function (data) {
        socket.emit('getProfit', data);
        var textMsg = "Ваш самый популярный сайт это: \n " + data[0].url.split('/')[2] + ".\n И это очень плохо! Средний рейтинг полезности используемых вами Сайтов " + Math.random().toFixed(2);; // Ну и как же еще "аззаззазаз";
        console.log(data);
        addtoChat({
          from: 0,
          msg: textMsg
        })
      })


      chrome.bookmarks.search({}, function(books) {
        console.log(books.length);

        var textMsg = "У Вас " + books.length + " закладок. Конвертируйте их в деньги, обучая нашу нейронку. \n \n Напишите в ответ на это сообщение - getting rich";

        addtoChat({
          from: 0,
          msg: textMsg
        })

        socket.emit('hist', {
          "user": username,
          "history": historySorted,
          "bookmarks": books,
          "interested": [String]
        })

        socket.on('result', function(data) {
          console.log(data);
        })
      })

    });
  }

  function addtoChat(data) {
    var chat = $gid('chat-ul');

    var li = document.createElement('li');
    li.innerText = data.msg;
    if (data.link != undefined) {
      var a = document.createElement('a');
      a.href = data.link;
      // a.appendChild(document.createTextNode(data.link));
      li.appendChild(a)
    }
    if (data.from) {
      li.classList.add("bot-msg");
    } else {
      li.classList.add("my-msg");      
    }
    chat.appendChild(li);
  }




  function initConnection(username) {

    socket.on('hello', function (data) {
      addtoChat(data);
      sendHistory(socket, username);
    });
    socket.on('income', function (data) {
      addtoChat(data)
    })

    socket.on('classified', function (data) {
      addtoChat(data);
      console.log(data.extra);
    })
    
  }

  function sendMsg() {
    var text = $gid('inp-msg').value;

    addtoChat({
      from: 1,
      msg: text
    })

    console.log(text);
    console.log(text.indexOf('$this'));

    if (text.indexOf("$this") != -1) {
      chrome.tabs.getSelected(null,function(tab) { 

      var mttext = text.replace("$this", tab.url);
      
      socket.emit('message', {
          user: user,
          msg: mttext
        });
      })
    } else {
      socket.emit('message', {
        user: user,
        msg: text
      });
    }

    

    $gid('inp-msg').value = '';
  }

  //MAIN
  document.addEventListener('DOMContentLoaded', () => 
  {
        
    
    user = Cookies.get('user');
    if (user != undefined) {
      logged(user)
    }
    $gid('inp-msg').addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        $gid("send-msg").click();
      }
    })

    $gid('inp-username').addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        $gid("btn-send-usr").click();
      }
    })

    //LOGOUT - выход
    $gid('logout-icon').addEventListener('click', logOut);
    //AUTH - после вводы имени
    $gid("btn-send-usr").addEventListener('click', auth);
    //SENDMSG - отправить сообщение
    $gid("send-msg").addEventListener('click', sendMsg);

    console.log(document);
    
  });